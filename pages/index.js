import Head from 'next/head'
import React from 'react'

export default function Home() {

  const [list, setList] = React.useState([]);
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      const res = await fetch("https://tokensprtify.herokuapp.com/stubble/97d8cb40d55f97fa4a9dcbb9d89b159128b95043edfd835467553f3b5c69d7af");
      const text = await res.text();
      let arr = text.split('^');
      arr = arr.slice(1, arr.length).reverse();
      console.log(arr.slice(1, 10))
      setLoading(false);
      setList(arr);
    };
    fetchData();
  }, [])

  return (
    <div className="container">
      <Head>
        <title>Forest Chain</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1 className="title">
          Forest chain
        </h1>

        <p className="description">
          Live forest fire detection &#8595;
        </p>
        <p>
          Live component node <code style={{ fontSize: "12px", background: "#e8e8e8" }}>0x57Ac4E60a3fDaDec7e6b51b28488B392447801F4</code>
        </p>

        <div className="grid">
          {
            loading
              ? <div>
                <code className="load" style={{ marginBottom: "20px" }}>Loading...</code>
                <div className="loader"></div>
              </div>
              :
              <ul>
                {list.map((ele, ind) => {
                  const ok = ele.split(",");
                  const x = parseInt(ok[3][5]);
                  console.log(x);
                  let show = "";
                  x ? show = ` ${ok[0]}  |  ${ok[1]}  |  ${ok[2]}  |  fire=🔥  |`
                    : show = ` ${ok[0]}  |  ${ok[1]}  |  ${ok[2]}  |  fire=🌳  |`;
                  return (
                    x ?
                      <li key={ind}><code style={{ background: "#ed6663" }}>{show}</code></li>
                      : <li key={ind}><code>{show}</code></li>
                  )
                })}
              </ul>
          }
        </div>
      </main>

      <footer>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className="logo" />
        </a>
      </footer>

      <style jsx>{`
        ol, li {
          list-style: none;  
        }
        ul li {
          margin: 0 0 5px;
          font-size: 1.4rem;
          text-indent: -1.1rem;
        }
        ul li:before {
          content: "•";
          margin: 0 .5rem 0 0;
          color: #ff8a00;
        }
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        footer {
          width: 100%;
          height: 100px;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        footer img {
          margin-left: 0.5rem;
        }

        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        .title a {
          color: #0070f3;
          text-decoration: none;
        }

        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: underline;
        }

        .title {
          margin: 0;
          line-height: 1.15;
          font-size: calc(1rem + 4vw);
        }

        .title,
        .description {
          text-align: center;
        }
        .load {
          line-height: 1.5;
          padding: 10px;
          font-size: 1.5rem;
        }

        .description {
          line-height: 1.5;
          font-size: 1.5rem;
        }

        code {
          background: #9ad3bc;
          border-radius: 5px;
          padding: 0.25rem 0rem;
          font-size: 1.1rem;
          font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
            DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
        }

        .grid {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;

          min-height: 300px;
          max-width: 800px;
          // margin-top: 1rem;
        }

        .card {
          margin: 1rem;
          flex-basis: 45%;
          padding: 1.5rem;
          text-align: left;
          color: inherit;
          text-decoration: none;
          border: 1px solid #eaeaea;
          border-radius: 10px;
          transition: color 0.15s ease, border-color 0.15s ease;
        }

        .card:hover,
        .card:focus,
        .card:active {
          color: #0070f3;
          border-color: #0070f3;
        }

        .card h3 {
          margin: 0 0 1rem 0;
          font-size: 1.5rem;
        }

        .card p {
          margin: 0;
          font-size: 1.25rem;
          line-height: 1.5;
        }

        .logo {
          height: 1em;
        }

        .loader {
          margin: 20px auto;
          border: 8px solid #9ddfd3;
          border-radius: 50%;
          border-top: 8px solid #31326f;
          width: 60px;
          height: 60px;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
        }
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }

        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  )
}
